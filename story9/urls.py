from django.urls import path
from . import views

app_name='story9'

urlpatterns = [
    path('story9/hallo',views.hallo, name='hallo'),
    path('story9/login',views.login_view, name='login_view'),
    path('story9/register',views.register_view, name='register_view'),
    path('story9/logout',views.logout_view, name='logout_view'),
]
