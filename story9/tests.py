from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import hallo, login_view, register_view

from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UnitTest(TestCase):

    def test_login_url_is_exist(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code, 200)

    def test_register_url_is_exist(self):
        response = Client().get('/story9/register')
        self.assertEqual(response.status_code, 200)


    def test_login_template_is_exist(self):
        response = Client().get('/story9/login')
        self.assertTemplateUsed(response, 'login.html')


    def test_register_template_is_exist(self):
        response = Client().get('/story9/register')
        self.assertTemplateUsed(response, 'signup.html')


class FuncionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncionalTest, self).tearDown()

    def test_functional(self):
        self.browser.get('http://localhost:8000/story9/login')
        self.browser.quit()
